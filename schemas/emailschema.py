from optparse import Option
from typing import List, Optional, Generic, TypeVar
from pydantic import BaseModel,Field,EmailStr
from pydantic.generics import GenericModel

T = TypeVar('T')

class EmailSchema(BaseModel):
    email: List[EmailStr]

    class Config:
        orm_mode = True

class RequestEmail(BaseModel):
    parameter: EmailSchema = Field(...)

class Response(GenericModel,Generic[T]):
    code: str
    status: str
    message: str

    