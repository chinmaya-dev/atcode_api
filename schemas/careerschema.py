from optparse import Option
from typing import List, Optional, Generic, TypeVar
from pydantic import BaseModel,Field
from pydantic.generics import GenericModel
from fastapi import File, UploadFile

T = TypeVar('T')

class CareerSchema(BaseModel):
    id: Optional[int]=None
    firstname: Optional[str]=None
    lastname: Optional[str]=None
    phone: Optional[str]=None
    email: Optional[str]=None
    file_name: Optional[str]=None

    class Config:
        orm_mode = True

class RequestCareer(BaseModel):
    parameter: CareerSchema = Field(...)

class Response(GenericModel):
    code: str
    status: str
    message: str

    class Config:
        orm_mode = True