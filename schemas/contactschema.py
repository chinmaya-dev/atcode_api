from optparse import Option
from typing import List, Optional, Generic, TypeVar
from pydantic import BaseModel,Field
from pydantic.generics import GenericModel

T = TypeVar('T')

class ContactSchema(BaseModel):
    id: Optional[int]=None
    name: Optional[str]=None
    company: Optional[str]=None
    email: Optional[str] = None
    phone: Optional[str] = None
    message: Optional[str] = None

    class Config:
        orm_mode = True

class RequestContact(BaseModel):
    parameter: ContactSchema = Field(...)

class Response(GenericModel,Generic[T]):
    code: str
    status: str
    message: str

    