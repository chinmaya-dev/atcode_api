import os
import secrets
import aiofiles



async def UploadImage(file):
    filename=file.filename
    extension=filename.split(".")[1]

    if extension not in ["pdf" , "docx", "doc"]:
        return {"status":"error","detail":"File extension not allowed"}

    token_name=secrets.token_hex(10)+"."+extension
    generated_name=token_name    
    filename = os.path.join('./app/static/images', generated_name) 
    async with aiofiles.open(filename, 'wb') as f:
        while content := await file.read(1024): # async read chunk
            await f.write(content)

    await file.close()

    return generated_name

        