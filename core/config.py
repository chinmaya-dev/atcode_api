from sqlalchemy import create_engine
from sqlalchemy.orm import sessionmaker
from sqlalchemy.ext.declarative import declarative_base

DATABASE_URL = "postgresql://postgres:postgres@localhost:5432/atcode_db"

engine=create_engine(DATABASE_URL)
SessionLocal= sessionmaker(autocommit=False, bind=engine)
Base = declarative_base()