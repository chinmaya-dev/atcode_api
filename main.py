from fastapi import FastAPI
import app.models.contact as models
from app.core.config import engine
import app.routers.router as router
import os
from fastapi.staticfiles import StaticFiles
from fastapi.middleware.cors import CORSMiddleware

models.Base.metadata.create_all(bind=engine)

app = FastAPI()

origins = ["*"]

app.add_middleware(
    CORSMiddleware,
    allow_origins=origins,
    allow_credentials=True,
    allow_methods=["*"],
    allow_headers=["*"],
)

script_dir = os.path.dirname(__file__)
st_abs_file_path = os.path.join(script_dir, "static\\")
#static file setup config
app.mount("/static", StaticFiles(directory=st_abs_file_path),name="static")

@app.get("/")
async def root():
    return {"message": "Hello World"}

app.include_router(router.router,prefix="/api/v1",tags=["contact"])
