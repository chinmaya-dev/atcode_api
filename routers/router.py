from fastapi import APIRouter, HTTPException, Path, Depends, status, File, UploadFile, Form
from sqlalchemy import true
from app.core.config import SessionLocal
from sqlalchemy.orm import Session
import app.crud.contact as contactcrud
from app.schemas.contactschema import RequestContact,Response
import app.crud.career as careercrud
from app.schemas.careerschema import CareerSchema,RequestCareer,Response
import app.core.uploadfile as uploadfileOP
from app.schemas.emailschema import EmailSchema

from fastapi_mail import FastMail, MessageSchema,ConnectionConfig
from starlette.requests import Request
from starlette.responses import JSONResponse
from typing import List

router =  APIRouter()


def get_db():
    db = SessionLocal()
    try:
        yield db
    finally:
        db.close()

@router.post('/create_contact')
async def create(request:RequestContact,db:Session=Depends(get_db)):
    _contact =contactcrud.create_contact(db,contact=request.parameter)
    if not _contact:
        raise HTTPException(status_code=422, detail="Record not created.")        
    else:
        return Response(code=200,status="Ok",message="Contact created sucess!!")

@router.post('/create_career')
async def create_file(firstname: str = Form(...), lastname: str = Form(...),email: str = Form(...),phone: str = Form(...),file: UploadFile = File(...),db:Session=Depends(get_db)):
    
    fileval=await uploadfileOP.UploadImage(file)
    new_item = CareerSchema(
        firstname=firstname,
        lastname=lastname,
        email=email,
        phone=phone,
        file_name=fileval
        )
        
    
    _career = careercrud.create_career(db,career=new_item)
    if not _career:
        raise HTTPException(status_code=422, detail="Record not created.")        
    else:
        return Response(code=200,status="Ok",message="Career created sucess!!")

@router.post('/send_mail')
async def send_mail(email: EmailSchema):
    conf = ConnectionConfig(
        MAIL_USERNAME="chinmaya.dfg@gmail.com",
        MAIL_PASSWORD="bithul@1992",
        MAIL_PORT=587,
        MAIL_SERVER="smtp.gmail.com",
        MAIL_TLS=True,
        MAIL_SSL=False
    )
    template = """
        <html>
        <body>
         
 
<p>Hi !!!
        <br>Thanks for using fastapi mail, keep using it..!!!</p>
 
 
        </body>
        </html>
        """
 
    message = MessageSchema(
        subject="Fastapi-Mail module",
        recipients=email.dict().get("email"),  # List of recipients, as many as you can pass
        body=template,
        subtype="html"
        )
 
    fm = FastMail(conf)
    await fm.send_message(message)
    print(message)
 
     
 
    return JSONResponse(status_code=200, content={"message": "email has been sent"})