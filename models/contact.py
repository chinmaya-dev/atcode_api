from sqlalchemy import Column, Integer, String
from app.core.config import Base

class Contact(Base):
    __tablename__ = "Contact"

    id=Column(Integer, index=True, primary_key=True, autoincrement=True)
    name=Column(String)
    company=Column(String)
    email = Column(String)
    phone = Column(String)
    message=Column(String)