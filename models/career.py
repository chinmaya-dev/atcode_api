from sqlalchemy import Column,ForeignKey,Integer, String, Float
from app.core.config import Base

class Career(Base):
    __tablename__ = "Career"

    id=Column(Integer, index=True, primary_key=True, autoincrement=True)
    firstname=Column(String)
    lastname=Column(String)
    phone=Column(String)
    email = Column(String)
    file_name=Column(String)