from sqlalchemy import false, true
from sqlalchemy.orm import Session
from app.models.career import Career
from app.schemas.careerschema import  CareerSchema

def create_career(db:Session, career: CareerSchema):
    _career = Career(firstname=career.firstname, lastname=career.lastname, phone=career.phone, email=career.email, file_name=career.file_name)
    db.add(_career)
    db.commit()
    db.refresh(_career)
    return _career