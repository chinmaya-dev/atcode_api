from sqlalchemy import false, true
from sqlalchemy.orm import Session
from app.models.contact import Contact
from app.schemas.contactschema import  ContactSchema

def create_contact(db:Session, contact: ContactSchema):
    _contact = Contact(name=contact.name,company=contact.company,email=contact.email,phone=contact.phone,message=contact.message)
    db.add(_contact)
    db.commit()
    db.refresh(_contact)
    return _contact